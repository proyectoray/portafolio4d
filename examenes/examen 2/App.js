import React, { useState, useEffect } from 'react';
import {
  TouchableOpacity,
  View,
  ActivityIndicator,
  SafeAreaView,
  StyleSheet,
  Image,
  Text,
  ScrollView,
  Dimensions,
} from 'react-native';
import { fetchTodos } from './api';
import TodoList from './TodoList';


const screenWidth = Dimensions.get('window').width;

const App = () => {
  const [isLoading, setLoading] = useState(true);
  const [todos, setTodos] = useState([]);
  const [filter, setFilter] = useState(null);

  useEffect(() => {
    fetchTodos()
      .then((todos) => setTodos(todos))
      .catch(console.error)
      .finally(() => setLoading(false));
  }, []);

  const filteredTodos = todos.filter((todo) => {
    switch (filter) {
      case 'completed':
        return todo.completed;
      case 'not-completed':
        return !todo.completed;
      default:
        return true; 
    }
  }).map((todo) => {
    switch (filter) {
      case 'ids':
        return { id: todo.id };
      case 'ids-titles':
        return { id: todo.id, title: todo.title };
      case 'ids-userids':
        return { id: todo.id, userId: todo.userId };
      case 'resolved-ids-userids':
        if (todo.completed) return { id: todo.id, userId: todo.userId };
        break;
      case 'unresolved-ids-userids':
        if (!todo.completed) return { id: todo.id, userId: todo.userId };
        break;
      default:
        return todo;
    }
  }).filter(Boolean);

  if (isLoading) return <ActivityIndicator />;

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.appContainer}>
        <ScrollView style={[styles.sidebar, { width: screenWidth * 0.25 }]}>
          <TouchableOpacity style={styles.button} onPress={() => setFilter(null)}>
            <Text style={styles.buttonText}>Mostrar Todos</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={() => setFilter('ids')}>
            <Text style={styles.buttonText}>Todos los pendientes (solo IDs)</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={() => setFilter('ids-titles')}>
            <Text style={styles.buttonText}>Todos los pendientes (IDs y Títulos)</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={() => setFilter('ids-userids')}>
            <Text style={styles.buttonText}>Todos los pendientes (IDs y UserID)</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={() => setFilter('not-completed')}>
            <Text style={styles.buttonText}>Pendientes sin resolver</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={() => setFilter('completed')}>
            <Text style={styles.buttonText}>Pendientes resueltos</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={() => setFilter('resolved-ids-userids')}>
            <Text style={styles.buttonText}>Resueltos (IDs y UserID)</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={() => setFilter('unresolved-ids-userids')}>
            <Text style={styles.buttonText}>Sin resolver (IDs y UserID)</Text>
          </TouchableOpacity>
        </ScrollView>
        <View style={styles.mainContent}>
          <View style={styles.header}>
            <Image
              source={require('./assets/nfl-logo.png')}
              style={styles.logo}
              resizeMode="contain"
            />
            <Text style={styles.title}>NFL</Text>
          </View>
          <TodoList todos={filteredTodos} />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#0A2342',
  },
  appContainer: {
    flexDirection: 'row',
    flex: 1,
  },
  sidebar: {
    backgroundColor: '#2A2A2A',
    padding: 20,
  },
  mainContent: {
    flex: 1,
    padding: 20,
  },
  button: {
    marginBottom: 10,
    backgroundColor: '#D50A0A',
    padding: 10,
    borderRadius: 5,
    alignItems: 'center',
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 16,
  },
  header: {
    alignItems: 'center',
    marginBottom: 20,
  },
  logo: {
    width: 120,
    height: 60,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginTop: 10,
  },
});

export default App;
