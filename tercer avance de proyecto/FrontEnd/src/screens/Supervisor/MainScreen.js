import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Alert,
} from "react-native";
import Background from "../../components/Background";
import Header from "../../components/Header";
import axios from "axios";
import { SERVER_IP } from "../../config";
import AsyncStorage from "@react-native-async-storage/async-storage";

const MainScreen = ({ navigation }) => {
  const [solicitudes, setSolicitudes] = useState([]);
  const [error, setError] = useState(null);

  const fetchSolicitudes = async () => {
    try {
      const token = await AsyncStorage.getItem("userToken");
      const userId = await AsyncStorage.getItem("userId");
      //console.log(userId);

      if (!token) {
        //console.error("Token no encontrado. Usuario no autenticado.");
        navigation.navigate("LoginScreen");
        return;
      }

      const response = await axios.get(
        `http://${SERVER_IP}:8000/api/Solicitud/${userId}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      if (Array.isArray(response.data.body)) {
        setSolicitudes(response.data.body);
      } else {
        console.error("La respuesta no contiene un arreglo de solicitudes");
        setSolicitudes([]);
      }
    } catch (error) {
      console.error("Error al obtener las solicitudes:", error);
      setError(error.message);
    }
  };

  const handleDeleteConfirmation = (solicitudId) => {
    Alert.alert(
      "Eliminar solicitud",
      "¿Estás seguro de que deseas eliminar esta solicitud?",
      [
        {
          text: "Cancelar",
          onPress: () => console.log("Cancelado"),
          style: "cancel",
        },
        {
          text: "Eliminar",
          onPress: () => handleDelete(solicitudId),
          style: "destructive",
        },
      ],
      { cancelable: false }
    );
  };

  const handleDelete = async (solicitudId) => {
    try {
      const token = await AsyncStorage.getItem("userToken");

      if (!token) {
        console.error("Token no encontrado. Usuario no autenticado.");
        navigation.navigate("LoginScreen");
        return;
      }

      await axios.delete(
        `http://${SERVER_IP}:8000/api/Solicitud/${solicitudId}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      fetchSolicitudes();
    } catch (error) {
      console.error("Error al eliminar la solicitud:", error);
      setError(error.message);
    }
  };

  useEffect(() => {
    const timer = setInterval(() => {
      fetchSolicitudes();
    }, 3000);
    return () => clearInterval(timer);
  }, []);

  return (
    <Background>
      <View style={styles.headerContainer}>
        <Header style={styles.header}>Mis Solicitudes</Header>
      </View>
      <ScrollView style={[styles.container, { marginBottom: 68 }]}>
        {error ? (
          <View style={styles.errorContainer}>
            <Text style={styles.errorText}>Error: {error}</Text>
          </View>
        ) : (
          solicitudes.map((solicitud, index) => (
            <View key={index} style={styles.card}>
              <Text style={styles.boldtitle}>
                Solicitud ID# {solicitud["Solicitud ID"]}
              </Text>
              <View style={styles.row}>
                <View style={styles.column}>
                  <Text style={styles.title}>Prioridad:</Text>
                  <Text style={styles.detail}>{solicitud["Prioridad"]}</Text>
                </View>
                <View style={styles.column}>
                  <Text style={styles.title}>Tipo de Mantenimiento:</Text>
                  <Text style={styles.detail}>
                    {solicitud["Tipo de mantenimiento"]}
                  </Text>
                </View>
              </View>
              <Text style={styles.title}>Descripción:</Text>
              <Text style={styles.detail}>
                {solicitud["Descripcion del problema"]}
              </Text>
              <View style={styles.row}>
                <View style={styles.column}>
                  <Text style={styles.title}>Fecha:</Text>
                  <Text style={styles.detail}>
                    {new Date(solicitud["Fecha"]).toLocaleDateString("es-ES")}
                  </Text>
                </View>
                <View style={styles.column}>
                  <Text style={styles.title}>Hora:</Text>
                  <Text style={styles.detail}>
                    {new Date(solicitud["Fecha"]).toLocaleTimeString("es-ES", {
                      hour: "2-digit",
                      minute: "2-digit",
                    })}
                  </Text>
                </View>
              </View>
              <View style={styles.row}>
                <View style={styles.column}>
                  <Text style={styles.title}>Departamento:</Text>
                  <Text style={styles.detail}>{solicitud["Departamento"]}</Text>
                </View>
                <View style={styles.column}>
                  <Text style={styles.title}>Máquina:</Text>
                  <Text style={styles.detail}>
                    {solicitud["Maquina"] !== ""
                      ? solicitud["Maquina"]
                      : "Sin asignar"}
                  </Text>
                </View>
              </View>
              <TouchableOpacity
                style={styles.button}
                onPress={() =>
                  handleDeleteConfirmation(solicitud?.["Solicitud ID"])
                }
              >
                <Text style={styles.buttonText}>Eliminar</Text>
              </TouchableOpacity>
            </View>
          ))
        )}
      </ScrollView>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginBottom: 70,
  },
  card: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    marginBottom: 16,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 4,
  },
  boldtitle: {
    fontSize: 24,
    fontFamily: "Gilroy-Bold",
    color: "#333",
    paddingBottom: 20,
  },
  title: {
    color: "#133036",
    fontSize: 16,
    fontFamily: "Gilroy-Bold",
    marginBottom: 5,
  },
  detail: {
    color: "#133036",
    fontSize: 16,
    fontFamily: "Gilroy-Regular",
    marginBottom: 10,
  },
  button: {
    backgroundColor: "#E54040",
    borderRadius: 30,
    paddingVertical: 15,
    paddingHorizontal: 20,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    width: "50%",
  },
  buttonText: {
    color: "white",
    fontSize: 18,
    fontFamily: "Gilroy-Bold",
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  column: {
    flex: 1,
  },
  // HEADER
  headerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: 120,
    paddingTop: 40,
    paddingHorizontal: 20,
    backgroundColor: "#133036",
  },
  header: {
    fontSize: 25,
    fontFamily: "Gilroy-Bold",
    color: "white",
  },
  // Estilos para el mensaje de error
  errorContainer: {
    backgroundColor: "#ffcccc",
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
  },
  errorText: {
    color: "#ff0000",
    fontSize: 16,
    fontFamily: "Gilroy-Bold",
  },
});

export default MainScreen;
