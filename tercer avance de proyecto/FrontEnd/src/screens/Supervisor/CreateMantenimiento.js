import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
  Text,
  TextInput,
  Alert,
  Platform,
} from "react-native";
import Background from "../../components/Background";
import Header from "../../components/Header";
import moment from "moment";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { SERVER_IP } from "../../config";

const CreateMantenimiento = ({}) => {
  const [description, setDescription] = useState("");
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [supervisorId, setSupervisorId] = useState(null);

  useEffect(() => {
    const fetchUserInfo = async () => {
      try {
        const userId = await AsyncStorage.getItem("userId");
        setSupervisorId(userId);
      } catch (error) {
        console.error("Error al obtener el ID del usuario:", error);
      }
    };

    fetchUserInfo();
  }, []);

  const currentDate = moment().format("YYYY-MM-DD");
  const currentTime = moment().format("HH:mm");
  useEffect(() => {
    setDate(currentDate);
    setTime(currentTime);
  }, []);

  const sendDataToAPI = async () => {
    try {
      const response = await axios.post(
        `http://${SERVER_IP}:8000/api/Mantenimiento`,
        {
          descripcion: description,
          fecha: date,
        }
      );
      console.log("Respuesta de la API:", response.data);
      return response;
    } catch (error) {
      console.error("Error al enviar datos:", error);
      throw error;
    }
  };

  const clearForm = () => {
    setDescription("");
  };

  const handleConfirm = () => {
    Alert.alert(
      "Confirmar",
      "¿Estás seguro de enviar este mantenimiento?",
      [
        {
          text: "Cancelar",
          style: "cancel",
        },
        {
          text: "Confirmar",
          onPress: async () => {
            try {
              const response = await sendDataToAPI();
              console.log("Datos enviados con éxito:", response.data);
              clearForm();
            } catch (error) {
              console.error("Error al enviar datos:", error);
            }
          },
        },
      ],
      { cancelable: false }
    );
  };

  const placeholderTextColor = Platform.OS === "ios" ? "#999" : "#555";

  return (
    <Background>
      <View style={styles.headerContainer}>
        <Header style={styles.header}>Crear Mantenimiento</Header>
      </View>
      <ScrollView style={styles.container}>
        <View style={styles.card}>
          <TextInput
            style={[styles.input, styles.inputDescripcion]}
            placeholder="Descripción del mantenimiento"
            placeholderTextColor={placeholderTextColor}
            value={description}
            onChangeText={setDescription}
            multiline
            textAlignVertical="top"
          />

          <View style={styles.inputRow}>
            <TextInput
              style={[styles.input, styles.inputFecha]}
              placeholder="Fecha"
              placeholderTextColor={placeholderTextColor}
              value={date}
              onChangeText={setDate}
              editable={false}
              textAlign="center"
            />
            <View style={{ width: 50 }} />
            <TextInput
              style={[styles.input, styles.inputHora]}
              placeholder="Hora"
              placeholderTextColor={placeholderTextColor}
              value={time}
              onChangeText={setTime}
              editable={false}
              textAlign="center"
            />
          </View>

          <TouchableOpacity style={styles.button} onPress={handleConfirm}>
            <Text style={styles.buttonText}>Confirmar</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginBottom: 0,
  },
  card: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    marginBottom: 16,
    marginTop: 30,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 1,
  },
  input: {
    fontFamily: "Gilroy-Regular",
    fontSize: 16,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: "#ddd",
    padding: 10,
    borderRadius: 6,
  },
  inputDescripcion: {
    height: 100,
    textAlignVertical: "top",
  },
  inputRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  inputFecha: {
    flex: 0.52,
  },
  inputHora: {
    flex: 0.57,
  },
  button: {
    backgroundColor: "#3DC253",
    borderRadius: 30,
    paddingVertical: 15,
    paddingHorizontal: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
  buttonText: {
    color: "white",
    fontSize: 18,
    fontFamily: "Gilroy-Bold",
  },
  headerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: 120,
    paddingTop: 40,
    paddingHorizontal: 20,
    backgroundColor: "#133036",
  },
  header: {
    fontSize: 25,
    fontFamily: "Gilroy-Bold",
    color: "white",
  },
});

export default CreateMantenimiento;
