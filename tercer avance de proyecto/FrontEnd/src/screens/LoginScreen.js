import React, { useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TouchableOpacity, StyleSheet, View, Alert } from 'react-native';
import { Text } from 'react-native-paper';
import Background from '../components/Background';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import { theme } from '../core/theme';
import { emailValidator } from '../helpers/emailValidator';
import { passwordValidator } from '../helpers/passwordValidator';
import { SERVER_IP } from '../config';
import axios from 'axios';

const ip = SERVER_IP;

export default function LoginScreen({ navigation }) {
  const [email, setEmail] = useState({ value: '', error: '' });
  const [password, setPassword] = useState({ value: '', error: '' });

  const handleLogin = async () => {
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);
  
    if (emailError || passwordError) {
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });
      return;
    }
  
    const url = `http://${ip}:8000/api/auth/login`;
  
    try {
      const response = await axios.post(url, {
        usuario_id: email.value,
        password: password.value,
      });
  
      const token = response.data.body;
  
      if (token) {
        await AsyncStorage.setItem('userToken', token); 
  
        await AsyncStorage.setItem('userId', email.value);
  
        const response2 = await axios.get(`http://${ip}:8000/api/users/${email.value}`, {
          headers: { Authorization: `Bearer ${token}` },
        });
  
        const userInfo = response2.data.body[0];
  
        if (userInfo.rol === "supervisor") {
          Alert.alert('Éxito', 'Inicio de sesión exitoso');
          navigation.navigate('MainScreen');
        } else if (userInfo.rol === "empleado_mantenimiento") {
          Alert.alert('Éxito', 'Inicio de sesión exitoso');
          navigation.navigate('SolicitudesScreen');
        } else if (userInfo.rol === "administrador") {
          Alert.alert('Éxito', 'Inicio de sesión exitoso');
          navigation.navigate('AdminHomeScreen');
        }
      } else {
        Alert.alert('Error', 'Falló el inicio de sesión, token no recibido');
      }
    } catch (error) {
      console.error(error);
      Alert.alert('Error', 'Falló el inicio de sesión, verifica tus credenciales o intenta más tarde');
    }
  };

  return (
    <Background>
      <Logo />
      <Header style={styles.header}>Bienvenido</Header>
      <TextInput
        label="Número de empleado"
        returnKeyType="next"
        value={email.value}
        onChangeText={(text) => setEmail({ value: text, error: '' })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
        textStyle={{ fontFamily: 'Gilroy-Light' }}
        style={styles.textInput}
      />
      <TextInput
        label="Contraseña"
        returnKeyType="done"
        value={password.value}
        onChangeText={(text) => setPassword({ value: text, error: '' })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
        style={styles.textInput}
      />
      <Button
        mode="contained"
        onPress={handleLogin}
        labelStyle={styles.buttonText} 
      >
        Iniciar Sesión
      </Button>
    </Background>
  );
}

const styles = StyleSheet.create({
  textInput: {
    marginVertical: 4, 
  },
  forgot: {
    fontSize: 14,
    color: theme.colors.secondary,
    fontFamily: 'Gilroy-ExtraBold',
  },
  header: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 30,
    color: '#3DC253',
    alignSelf: 'center',
  },
  buttonText: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 15,
    fontWeight: '200', 
  },
});
