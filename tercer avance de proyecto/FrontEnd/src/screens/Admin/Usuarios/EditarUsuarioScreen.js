import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, TextInput } from "react-native";
import { SERVER_IP } from "../../../config";
import Axios from "axios";
import RNPickerSelect from "react-native-picker-select";

const url = `http://${SERVER_IP}:8000/api/users`;

export default class EditarUsuarioScreen extends Component {
    constructor(props) {
        super(props);
        const { id, nombrePila, apPat, apMat, rol } = this.props.route.params;
        this.state = {
            id: id,
            nombrePila: nombrePila,
            apPat: apPat,
            apMat: apMat,
            rol: rol,
            password: '', 
            confirmPassword: '' 
        };
    }

    componentDidMount() {
        const { route } = this.props;
        const { id, obtenerUsuario } = route.params; 
        obtenerUsuario(id); 
    }
    

    manejarCambioTexto = (campo, valor) => {
        this.setState({ [campo]: valor });
    }

    guardarCambios = () => {
        const { id, nombrePila, apPat, apMat, rol, password, confirmPassword } = this.state;
    
        if (password === confirmPassword) {
            const nuevoUsuario = {
                id: id,
                nombrePila: nombrePila,
                apPat: apPat,
                apMat: apMat,
                rol: rol,
                password: password,
                activo:1,
            };
    
            Axios.post(`${url}`, nuevoUsuario)
                .then((response) => {
                    if (response.status === 200) {
                        console.log("Usuario actualizado correctamente");
                        this.props.route.params.obtenerUsuario(id);
                        this.props.navigation.goBack();
                    } else {
                        console.log("Error al actualizar el usuario");
                    }
                })
                .catch((error) => {
                    console.log("Error al realizar la solicitud:", error);
                });
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.header}>Editar Usuario</Text>
                <TextInput
                    style={styles.input}
                    placeholder="Nombre(s)"
                    value={this.state.nombrePila}
                    onChangeText={(text) => this.setState({ nombrePila: text })}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Primer Apellido"
                    value={this.state.apPat}
                    onChangeText={(text) => this.setState({ apPat: text })}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Segundo Apellido"
                    value={this.state.apMat}
                    onChangeText={(text) => this.setState({ apMat: text })}
                />
                <View style={styles.pickerContainer}>
                    <RNPickerSelect
                        style={pickerSelectStyles}
                        placeholder={{
                            label: 'Rol',
                            value: null,
                        }}
                        onValueChange={(value) => this.setState({ rol: value })}
                        items={[
                            { label: 'Administrador', value: 'administrador' },
                            { label: 'Supervisor', value: 'supervisor' },
                            { label: 'Empleado', value: 'empleado_mantenimiento' },
                        ]}
                        value={this.state.rol}
                    />
                </View>
                <TextInput
                    style={styles.input}
                    placeholder="Nueva Contraseña"
                    value={this.state.password}
                    onChangeText={(text) => this.setState({ password: text })}
                    secureTextEntry={true} 
                />
                <TextInput
                    style={styles.input}
                    placeholder="Confirmar Contraseña"
                    value={this.state.confirmPassword}
                    onChangeText={(text) => this.setState({ confirmPassword: text })}
                    secureTextEntry={true}
                />
                <TouchableOpacity onPress={this.guardarCambios}>
                    <Text style={styles.button}>Guardar Cambios</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 5,
        padding: 10,
        marginBottom: 10,
        width: '80%',
    },
    pickerContainer: {
        width: '80%',
        marginBottom: 10,
    },
    button: {
        backgroundColor: '#007AFF',
        color: 'white',
        padding: 10,
        borderRadius: 5,
        marginTop: 10,
        width: '80%',
        textAlign: 'center',
    },
    header: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 20,
    }
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 5,
        color: 'black',
        paddingRight: 30,
        width: '100%',
        marginBottom: 10,
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 5,
        color: 'black',
        paddingRight: 30,
        width: '100%',
        marginBottom: 10,
    },
});
