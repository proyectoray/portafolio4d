import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, Button, TouchableOpacity, Alert } from 'react-native';
import Axios from 'axios';
import { SERVER_IP } from '../../../config';
import RNPickerSelect from 'react-native-picker-select';

const url = `http://${SERVER_IP}:8000/api/users`;

export default class AgregarUsuariosScreen extends Component {
  state = {
    nuevoUsuario: {
      id: 0,
      nombrePila: '',
      apPat: '',
      apMat: '',
      rol: '',
      activo: 1,
      password: '',
    },
  };

  manejarCambioTexto = (campo, valor) => {
    this.setState((prevState) => ({
      nuevoUsuario: {
        ...prevState.nuevoUsuario,
        [campo]: valor,
      },
    }));
  };

  agregarUsuario = async () => {
    try {
      const nuevoUsuario = { ...this.state.nuevoUsuario };
      const response = await Axios.post(url, nuevoUsuario);
      console.log(response.data.body.id);
      if (response.status === 200 || response.status === 201) {
        console.log('Usuario agregado correctamente');
        Alert.alert('Usuario agregado correctamente');
        this.props.navigation.navigate('Home');
      } else {
        console.error('Error al agregar usuario:', response);
      }
    } catch (error) {
      console.error('Error al agregar usuario 2:', error);
    }
  };
  


  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.header}>Agregar nuevo usuario</Text>
        <View style={styles.formContainer}>
        <TextInput
          style={styles.input}
          label="Nombre"
          placeholder="Nombre(s)"
          value={this.state.nuevoUsuario.nombrePila}
          onChangeText={(text) => this.manejarCambioTexto('nombrePila', text)}
        >
        </TextInput>
          <TextInput
            style={styles.input}
            label="Primer Apellido"
            placeholder="Apellido Paterno"
            value={this.state.nuevoUsuario.apPat}
            onChangeText={(text) => this.manejarCambioTexto('apPat', text)}
          />
          <TextInput
            style={styles.input}
            label="Segundo Apellido"
            placeholder="Apellido Materno"
            value={this.state.nuevoUsuario.apMat}
            onChangeText={(text) => this.manejarCambioTexto('apMat', text)}
          />
          <RNPickerSelect
            style={pickerSelectStyles}
            placeholder={{
              label: 'Rol de usuario',
              value: null,
            }}
            onValueChange={(value) => this.manejarCambioTexto('rol', value)}
            items={[
              { label: 'Empleado', value: 'empleado_mantenimiento' },
              { label: 'Supervisor', value: 'supervisor' },
              { label: 'Administrador', value: 'administrador' },
            ]}
          />
          <TextInput
            style={styles.input}
            label="Contraseña"
            placeholder="Contraseña"
            value={this.state.nuevoUsuario.password}
            onChangeText={(text) => this.manejarCambioTexto('password', text)}
            secureTextEntry
          />
          <TouchableOpacity
            style={styles.button}
            onPress={this.agregarUsuario}
          >
            <Text style={styles.buttonText}>Agregar usuario</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5',
  },
  header: {
    fontSize: 22,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  formContainer: {
    width: '80%',
    padding: 20,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
  },
  input: {
    height: 40,
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 5,
    marginVertical: 5,
    paddingHorizontal: 10,
    paddingVertical: 8, 
    textAlignVertical: 'top', 
  },
  button: {
    backgroundColor: '#007AFF',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
  },
  buttonText: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 16,
  },
  placeholder: {
    color: '#aaa',
    fontSize: 16,
  }
    
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, 
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, 
  },
});
