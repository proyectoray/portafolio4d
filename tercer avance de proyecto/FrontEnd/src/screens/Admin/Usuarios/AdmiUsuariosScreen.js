import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import Axios from 'axios';
import { SERVER_IP } from '../../../config';


const url = `http://${SERVER_IP}:8000/api/users`;

export default class AdminUsuariosScreen extends Component {
  state = {
    usuarios: [],
  };

  componentDidMount() {
    this.cargarUsuarios();
    this.focusListener = this.props.navigation.addListener('focus', () => {
        this.cargarUsuarios();
    });
}

componentWillUnmount() {
    this.focusListener();
}


cargarUsuarios = async () => {
    try {
        const response = await Axios.get(url);
        this.setState({ usuarios: response.data.body });
    } catch (error) {
        console.error('Error al obtener usuarios:', error);
    }
};

  render() {
    const { navigation } = this.props;

    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.usuarios}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item }) => (
            <View style={styles.usuarioContainer}>
              <Text style={styles.usuarioText}>{` ${item.nombrePila} ${item.apPat} ${item.apMat}`}</Text>
              <TouchableOpacity
                style={styles.botonGestionar}
                onPress={() => navigation.navigate('Gestionar Usuario', { id: item.id })}
              >
                <Text style={styles.textoBoton}>Gestionar</Text>
              </TouchableOpacity>
            </View>
          )}
        />
        <View style={styles.nuevoUsuarioContainer}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('Agregar Usuario')}
          >
            <Text style={styles.buttonText}>Agregar usuario</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  usuarioContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 15,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    marginHorizontal: 10,
    marginVertical: 5,
  },
  usuarioText: {
    fontSize: 16,
  },
  botonGestionar: {
    backgroundColor: '#007AFF',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 5,
  },
  textoBoton: {
    color: '#fff',
    fontWeight: 'bold',
  },
  nuevoUsuarioContainer: {
    marginVertical: 20,
    alignItems: 'center',
  },
  button: {
    backgroundColor: '#007AFF',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 16,
  },
});