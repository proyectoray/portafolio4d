import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

export default class AdminHomeScreen extends Component {
  render() {
    const { navigation } = this.props; 

    return (
      <View style={styles.container}>
        <Text style={styles.header}>Master Fix Admin</Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Usuarios')}
        >
          <Text style={styles.buttonText}>Usuarios</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5',
  },
  header: {
    fontSize: 22,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  button: {
    backgroundColor: '#007AFF',
    padding: 10,
    borderRadius: 5,
    marginTop: 10,
    width: 200,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});



