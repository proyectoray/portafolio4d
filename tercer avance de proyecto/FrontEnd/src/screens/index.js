//Vistas inicial
export { default as LoginScreen } from './LoginScreen'
//Vista Web
export { default as WebScreen } from './WebScreen' 
//Vistas Empleado
export { default as SolicitudesScreen } from './Empleados/SolicitudesScreen'
export { default as TareasScreen} from './Empleados/TareasScreen'
export { default as ProfileScreen } from './Empleados/ProfileScreen'
export { default as CompletadasScreen } from './Empleados/CompletadasScreen'

