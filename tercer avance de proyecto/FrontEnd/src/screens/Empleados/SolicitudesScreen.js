import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Alert,
} from "react-native";
import axios from "axios";
import Background from "../../components/Background";
import Header from "../../components/Header";
import { SERVER_IP } from "../../config";
import AsyncStorage from "@react-native-async-storage/async-storage";

const SolicitudesScreen = ({ navigation }) => {
  const [solicitudes, setSolicitudes] = useState([]);
  const [selectedSolicitudId, setSelectedSolicitudId] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetchSolicitudes();
  }, [navigation]);

  const fetchSolicitudes = async () => {
    try {
      const token = await AsyncStorage.getItem("userToken");

      if (!token) {
        //console.error("Token no encontrado. Usuario no autenticado.");
        navigation.navigate("LoginScreen");
        return;
      }

      const response = await axios.get(
        `http://${SERVER_IP}:8000/api/Solicitud`,
        {
          activo: 1,
        },

        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      if (Array.isArray(response.data.body)) {
        setSolicitudes(response.data.body);
      } else {
        console.error("La respuesta no contiene un arreglo de solicitudes");
      }
    } catch (error) {
      console.error("Error al obtener las solicitudes:", error);
      setError(error.message);
    }
  };

  const [accepting, setAccepting] = useState(false);

  const handleAccept = async (solicitudId) => {
    try {
      if (accepting) {
        console.log("Ya se está procesando una solicitud de aceptación.");
        return;
      }

      setAccepting(true);

      const token = await AsyncStorage.getItem("userToken");
      const userId = await AsyncStorage.getItem("userId");

      if (!token) {
        console.error("Token no encontrado. Usuario no autenticado.");
        navigation.navigate("LoginScreen");
        return;
      }

      if (!solicitudId) {
        console.error("No se ha seleccionado ninguna solicitud.");
        return;
      }

      await axios.post(
        `http://${SERVER_IP}:8000/api/Solicitud`,
        {
          usuario_id_resuelve: userId,
          activo: 0,
          id: solicitudId,
        },
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      fetchSolicitudes();

      Alert.alert("Solicitud aceptada correctamente");
    } catch (error) {
      console.error("Error al aceptar la solicitud:", error);
      setError(error.message);
    } finally {
      setAccepting(false);
    }
  };

  useEffect(() => {
    const timer = setInterval(() => {
      fetchSolicitudes();
    }, 3000);
    return () => clearInterval(timer);
  }, []);

  return (
    <Background>
      <View style={styles.headerContainer}>
        <Header style={styles.header}>Solicitudes disponibles</Header>
      </View>
      <ScrollView style={[styles.container, { marginBottom: 80 }]}>
        {error ? (
          <View style={styles.errorContainer}>
            <Text style={styles.errorText}>Error: {error}</Text>
          </View>
        ) : (
          solicitudes.map((solicitud, index) => (
            <View key={index} style={styles.card}>
              <Text style={styles.boldtitle}>
                Solicitud ID# {solicitud["Solicitud ID"]}
              </Text>
              <View style={styles.row}>
                <View style={styles.column}>
                  <Text style={styles.title}>Prioridad:</Text>
                  <Text style={styles.detail}>{solicitud["Prioridad"]}</Text>
                </View>
                <View style={styles.column}>
                  <Text style={styles.title}>Tipo de Mantenimiento:</Text>
                  <Text style={styles.detail}>
                    {solicitud["Tipo de mantenimiento"]}
                  </Text>
                </View>
              </View>
              <Text style={styles.title}>Descripción:</Text>
              <Text style={styles.detail}>
                {solicitud["Descripcion del problema"]}
              </Text>
              <View style={styles.row}>
                <View style={styles.column}>
                  <Text style={styles.title}>Fecha:</Text>
                  <Text style={styles.detail}>
                    {new Date(solicitud["Fecha"]).toLocaleDateString("es-ES")}
                  </Text>
                </View>
                <View style={styles.column}>
                  <Text style={styles.title}>Hora:</Text>
                  <Text style={styles.detail}>
                    {new Date(solicitud["Fecha"]).toLocaleTimeString("es-ES", {
                      hour: "2-digit",
                      minute: "2-digit",
                    })}
                  </Text>
                </View>
              </View>
              <View style={styles.row}>
                <View style={styles.column}>
                  <Text style={styles.title}>Departamento:</Text>
                  <Text style={styles.detail}>{solicitud["Departamento"]}</Text>
                </View>
                <View style={styles.column}>
                  <Text style={styles.title}>Máquina:</Text>
                  <Text style={styles.detail}>
                    {solicitud["Maquina"]}
                  </Text>
                </View>
              </View>
              <TouchableOpacity
                style={styles.button}
                onPress={() => handleAccept(solicitud?.["Solicitud ID"])}
              >
                <Text style={styles.buttonText}>Aceptar</Text>
              </TouchableOpacity>
            </View>
          ))
        )}
      </ScrollView>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginBottom: 80,
  },
  card: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    marginBottom: 16,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 1,
  },
  boldtitle: {
    fontSize: 24,
    fontFamily: "Gilroy-Bold",
    color: "#333",
    paddingBottom: 20,
  },
  title: {
    color: "#133036",
    fontSize: 16,
    fontFamily: "Gilroy-Bold",
    marginBottom: 5,
  },
  detail: {
    color: "#133036",
    fontSize: 16,
    fontFamily: "Gilroy-Regular",
    marginBottom: 10,
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  column: {
    flex: 1,
  },
  button: {
    backgroundColor: "#3DC253",
    borderRadius: 30,
    paddingVertical: 15,
    paddingHorizontal: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
  buttonText: {
    color: "white",
    fontSize: 18,
    fontFamily: "Gilroy-Bold",
  },
  headerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: 120,
    paddingTop: 40,
    paddingHorizontal: 20,
    backgroundColor: "#133036",
  },
  header: {
    fontSize: 25,
    fontFamily: "Gilroy-Bold",
    color: "white",
  },
  errorContainer: {
    backgroundColor: "#ffcccc",
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
  },
  errorText: {
    color: "#ff0000",
    fontSize: 16,
    fontFamily: "Gilroy-Bold",
  },
});

export default SolicitudesScreen;
