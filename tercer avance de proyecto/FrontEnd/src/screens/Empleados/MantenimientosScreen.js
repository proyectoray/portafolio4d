import React, { useState } from 'react';
import { StyleSheet, View, TouchableOpacity, Text, ScrollView } from 'react-native';
import Background from '../../components/Background';
import Header from '../../components/Header';
import TextInput from '../../components/TextInput'; 
import Icon from 'react-native-vector-icons/FontAwesome';

const MantenimientosScreen = ({ navigation }) => {
  const [maintenanceType, setMaintenanceType] = useState('');
  const [date, setDate] = useState('');
  const [place, setPlace] = useState('');
  const [machineNumber, setMachineNumber] = useState('');
  const [maintenanceDescription, setMaintenanceDescription] = useState('');

  const logout = () => {
    navigation.navigate('LoginScreen');
  };

  const acceptMaintenance = () => {
    console.log({
      maintenanceType,
      date,
      place,
      machineNumber,
      maintenanceDescription,
    });
  };

  return (
    <Background>
      <View style={styles.headerContainer}>
        <Header style={styles.header}>Mantenimientos disponibles</Header>
      </View>
      <ScrollView contentContainerStyle={styles.container}>
      </ScrollView>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textInput: {
    marginVertical: 2,
  },
  button: {
    backgroundColor: '#3DC253',
    padding: 15,
    marginTop: 10,
    borderRadius: 5,
    width: '60%',
    alignItems: 'center',
    alignSelf: 'center',
  },
  buttonText: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 15,
    color: 'white',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    height: 120,
    paddingTop: 40,
    paddingHorizontal: 20,
    backgroundColor: '#133036',
  },
  header: {
    fontSize: 25,
    fontFamily: 'Gilroy-Bold',
    color: 'white',
  },
});

export default MantenimientosScreen;
