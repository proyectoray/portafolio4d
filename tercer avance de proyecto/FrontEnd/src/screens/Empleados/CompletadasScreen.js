import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
} from "react-native";
import axios from "axios";
import Background from "../../components/Background";
import Header from "../../components/Header";
import { SERVER_IP } from "../../config";
import AsyncStorage from "@react-native-async-storage/async-storage";

const CompletadasScreen = ({ navigation }) => {
  const [taskDetails, setTaskDetails] = useState([]);
  const [error, setError] = useState(null);

  const fetchData = async () => {
    try {
      const token = await AsyncStorage.getItem("userToken");

      if (!token) {
        navigation.navigate("LoginScreen");
        return;
      }

      const response = await axios.get(
        `http://${SERVER_IP}:8000/api/Tarea`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      if (Array.isArray(response.data.body)) {
        setTaskDetails(response.data.body);
      } else {
        console.error("La respuesta no contiene un arreglo de tareas");
        setTaskDetails([]);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
      setError(error.message);
    }
  };

  

  useEffect(() => {
    fetchData();
  }, [navigation]);

  useEffect(() => {
    const timer = setInterval(() => {
      fetchData();
    }, 3000); 
    return () => clearInterval(timer);
  }, []);

  return (
    <Background>
      <View style={styles.headerContainer}>
        <Header style={styles.header}>Tareas Completadas</Header>
      </View>
      <ScrollView style={[styles.container, { marginBottom: 80 }]}>
        {error ? (
          <View style={styles.errorContainer}>
            <Text style={styles.errorText}>Error: {error}</Text>
          </View>
        ) : (
          taskDetails.map((task, index) => (
            <View key={index} style={styles.card}>
              <Text style={styles.boldtitle}>Tarea #{task.id}</Text>
              <View style={styles.row}>
                <View style={styles.column}>
                  <Text style={styles.title}>Prioridad:</Text>
                  <Text style={styles.detail}>{task.Prioridad}</Text>
                </View>
              </View>
              <Text style={styles.title}>Descripción:</Text>
              <Text style={styles.detail}>
                {task.Descripcion}
              </Text>
              <View style={styles.row}>
                <View style={styles.column}>
                  <Text style={styles.title}>Maquina:</Text>
                  <Text style={styles.detail}>{task.Maquina}</Text>
                </View>
                <View style={styles.column}>
                  <Text style={styles.title}>Número de Serie:</Text>
                  <Text style={styles.detail}>{task["Numero de Serie"]}</Text>
                </View>
              </View>
            </View>
          ))
        )}
      </ScrollView>
    </Background>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginBottom: 80,
  },
  card: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    marginBottom: 16,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 1,
  },  
  boldtitle: {
    fontSize: 24,
    fontFamily: "Gilroy-Bold",
    color: "#333",
    paddingBottom: 20,
  },
  title: {
    fontFamily: "Gilroy-Bold",
    fontSize: 16,
    color: "#133036",
  },
  detail: {
    fontFamily: "Gilroy-Regular",
    fontSize: 16,
    color: "#133036",
    marginBottom: 5,
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  column: {
    flex: 1,
  },
  button: {
    backgroundColor: "#3DC253",
    borderRadius: 30,
    paddingVertical: 15,
    paddingHorizontal: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
  buttonText: {
    color: "white",
    fontSize: 18,
    fontFamily: "Gilroy-Bold",
  },
  // HEADER
  headerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: 120,
    paddingTop: 40,
    paddingHorizontal: 20,
    backgroundColor: "#133036",
  },
  header: {
    fontSize: 25,
    fontFamily: "Gilroy-Bold",
    color: "white",
  },
  // Estilos para el mensaje de error
  errorContainer: {
    backgroundColor: "#ffcccc",
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
  },
  errorText: {
    color: "#ff0000",
    fontSize: 16,
    fontFamily: "Gilroy-Bold",
  },
});

export default CompletadasScreen;
