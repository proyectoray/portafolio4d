import { DefaultTheme } from 'react-native-paper';

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    text: '#133036',
    primary: '#133036',
    secondary: '#414757',
    error: '#f13a59',
  },
  fonts: {
    ...DefaultTheme.fonts,
    regular: {
      fontFamily: 'Gilroy-Light',
      fontWeight: 'normal',
    },
  },
};

export const COLORS = {
  primary: '#133036',
  white: "#FFFFFF",
  gray: "#ECF0F4",
};