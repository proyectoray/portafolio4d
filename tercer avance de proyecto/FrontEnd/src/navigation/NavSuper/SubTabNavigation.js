import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Image, Platform} from "react-native";
import { COLORS } from "../../core/theme";
import icons from "../../core/icons";
import {
  ProfileScreen,
} from "../../screens";
import CreateSolicitud from "../../screens/Supervisor/CreateSolicitud";
import MainScreen from "../../screens/Supervisor/MainScreen";

const Tab = createBottomTabNavigator();

const screenOptions = {
  tabBarShowLabel: true,
  headerShown: false,
  tabBarStyle: {
    position: "absolute",
    bottom: 0,
    right: 0,
    left: 0,
    elevation: 0,
    height: Platform.OS === "ios" ? 90 : 55,
    backgroundColor: COLORS.white,
  },
  tabBarLabelStyle: { 
    fontSize: 12,
    fontFamily: 'Gilroy-Bold',
    marginTop: -3,
    marginBottom: 5,
  },
  tabBarActiveTintColor: '#133036'
};

const SubTabNavigation = () => {
  return (
    <Tab.Navigator screenOptions={screenOptions}>
      <Tab.Screen
        name="MainScreen"
        component={MainScreen}
        options={{
          tabBarLabel: () => null,
          tabBarIcon: ({ focused }) => {
            return (
              <Image
                source={focused ? icons.solicitud : icons.solicitudOutline}
                resizeMode="contain"
                style={{
                  height: 24,
                  width: 24,
                  tintColor: focused ? COLORS.primary : COLORS.black,
                }}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="CreateSolicitud"
        component={CreateSolicitud}
        options={{
          tabBarLabel: () => null, 
          tabBarIcon: ({ focused }) => {
            return (
              <Image
                source={focused ? icons.anadir : icons.anadirOutline}
                resizeMode="contain"
                style={{
                  height: 24,
                  width: 24,
                  tintColor: focused ? COLORS.primary : COLORS.black,
                }}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="ProfileScreen"
        component={ProfileScreen}
        options={{
          tabBarLabel: () => null,
          tabBarIcon: ({ focused }) => {
            return (
              <Image
                source={focused ? icons.usuario : icons.usuarioOutline}
                resizeMode="contain"
                style={{
                  height: 24,
                  width: 24,
                  tintColor: focused ? COLORS.primary : COLORS.black,
                }}
              />
            );
          },
        }}
      />
    </Tab.Navigator>
  );
};

export default SubTabNavigation;
