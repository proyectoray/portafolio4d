import React from "react";
import { Provider } from "react-native-paper";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { theme } from "../core/theme";
import DrawerNavigation from "./NavEmple/DrawerNavigation";
import { LoginScreen } from "../screens";
import SubTabNavigation from "./NavSuper/SubTabNavigation";
import AdminTabNavigation from "./NavAdmin/AdminTavNavigation";

const Stack = createStackNavigator();

const AppNavigation = () => {
  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="LoginScreen"
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
          <Stack.Screen name="SolicitudesScreen" component={DrawerNavigation} />
          <Stack.Screen name="MainScreen" component={SubTabNavigation} />
          <Stack.Screen name="AdminHomeScreen" component={AdminTabNavigation} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default AppNavigation;
