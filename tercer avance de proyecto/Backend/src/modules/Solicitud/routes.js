const express = require("express");
const response = require("../../connect/response");
const driver = require("./index");

const router = express.Router();

router.get("/", all);
router.get("/:id", selectSolicitud);
router.put("/", disable);
router.post("/", add);
router.delete("/:id", eraseOne);

async function all(req, res, next) {
  try {
    const items = await driver.selectAllSolicitud();
    response.success(req, res, items, 200);
  } catch (error) {
    next(error);
    console.log(error);
  }
}

async function selectSolicitud(req, res, next) {
  try {
    const id = req.params.id;
    const items = await driver.selectSolicitud(id);
    response.success(req, res, items, 200);
  } catch (error) {
    next(error);
  }
}

async function add(req, res, next) {
  try {
    const items = await driver.add(req.body);
    if (req.body.id == 0) {
      response.success(req, res, "Item agregado", 200);
    } else {
      response.success(req, res, "Item actualizado", 200);
    }
  } catch (error) {
    next(error);
  }
}

async function disable(req, res, next) {
  try {
    const items = await driver.disable(req.body);
    response.success(req, res, "Item desactivado", 200);
  } catch (error) {
    next(error);
  }
}

async function eraseOne(req, res, next) {
  try {
    const id = req.params.id;
    await driver.eraseone({ id });
    response.success(req, res, "Solicitud eliminada", 200);
  } catch (error) {
    next(error);
  }
}

module.exports = router;
