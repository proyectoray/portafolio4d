const response = require('../../connect/response');

const auth = require('../auth');

const TABLE = 'Reporte';


module.exports = function(dbInyector){

    let db = dbInyector;

    if(!db){
        db = require('../../DB/mysql');
    }

    function all(){
        return db.all(TABLE)
    }
    
    function one(id){
        return db.one(TABLE, id)
    }

    function add(body){
        return db.add(TABLE, body)
    }
    
    
    function eraseone(body){
        return db.eraseone(TABLE, body)
    }

    function disable(body){
        return db.disable(TABLE, body)
    }

    function selectReporte(id){
        return db.selectReporte(TABLE, id)
    }

    function selectAllReporte(){
        return db.selectAllReporte(TABLE)
    }
    
    return {
        all,
        one,
        add,
        eraseone,
        disable,
        selectReporte,
        selectAllReporte
    }
}