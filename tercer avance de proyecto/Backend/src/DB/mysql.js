const mysql = require('mysql');
const config = require('../config');



const dbconfig = {
    host: config.mysql.host,
    user: config.mysql.user,
    password: config.mysql.password,
    database: config.mysql.database
};


let conexion;

function conmysql(){
    conexion = mysql.createConnection(dbconfig);
    return new Promise((resolve, reject) => {
        if (conexion) {
            console.log('Conectado a la base de datos');
            resolve();
        } else {
            console.log('No se pudo conectar a la base de datos');
            reject();

        }
    });
}

conmysql();

function all(table) { 
    return new Promise((resolve, reject) => {
        if (conexion) {
            const sql = `SELECT * FROM ${table} WHERE activo = 1`;
            const query = conexion.query(sql, (err, result) => {
                if (err) reject(err);
                resolve(result);
            });
        }        
    })
}

function one(table, id) { 
    return new Promise((resolve, reject) => {
        conexion.query(`SELECT * FROM ${table} WHERE id = ${id}`, (err, result) => {
            return err ? reject(err) : resolve(result)
        })
    })
}

function add(table, data) { 
    return new Promise((resolve, reject) => {
        conexion.query(`INSERT INTO ${table} SET ? ON DUPLICATE KEY UPDATE ?`, [data, data], (err, result) => {
            return err ? reject(err) : resolve(result)
        })
    })
}


function eraseone(table, data) { 
    return new Promise((resolve, reject) => {
        conexion.query(`DELETE FROM ${table} WHERE id = ?`, [data.id], (err, result) => {
            return err ? reject(err) : resolve(result)
        })
    })
}

function disable(table, data) { 
    return new Promise((resolve, reject) => {
        conexion.query(`UPDATE ${table} SET activo = 0 WHERE id = ?`, [data.id], (err, result) => {
            return err ? reject(err) : resolve(result)
        })
    })
}

function query(TABLE, consult) { 
    return new Promise((resolve, reject) => {
        conexion.query(`SELECT * FROM ${TABLE} WHERE ?`, consult, (err, result) => {
            return err ? reject(err) : resolve(result[0])
        })     
    })

}

function changePassword(TABLE, data) { 
    return new Promise((resolve, reject) => {
        conexion.query(`UPDATE ${TABLE} SET password = ? WHERE id = ?`, [data.password, data.id], (err, result) => {
            return err ? reject(err) : resolve(result)
        })
    })
}

function selectEmpleado(TABLE, id) {
    return new Promise((resolve, reject) => {
        const sql = `
        SELECT * FROM ${TABLE} WHERE activo = 1 AND id = ${id}
        `;
        conexion.query(sql, [id], (err, results) => {
            if (err) {
                reject(err);
            } else {
                resolve(results);
            }
        });
    });
}

function selectSupervisor(id) {
    return new Promise((resolve, reject) => {
        const sql = `
        SELECT 
        Sup.usuario_id,
        User.nombrePila,
        Sup.departamento_id,
        Depto.nombre
        FROM Supervisor AS Sup
        INNER JOIN Departamento AS Depto ON Depto.id = Sup.departamento_id
        INNER JOIN Usuario AS User ON User.id = Sup.usuario_id
        WHERE User.activo = 1;
        `;
        conexion.query(sql, [id], (err, results) => {
            if (err) {
                reject(err);
            } else {
                resolve(results);
            }
        });
    });
}

function selectAllSupervisor() {
    return new Promise((resolve, reject) => {
      const sql = `
        SELECT
          Sup.usuario_id,
          User.nombrePila,
          Sup.departamento_id,
          Depto.nombre
        FROM Supervisor AS Sup
        INNER JOIN Departamento AS Depto ON Depto.id = Sup.departamento_id
        INNER JOIN Usuario AS User ON User.id = Sup.usuario_id
        WHERE User.activo = 1;
      `;
  
      conexion.query(sql, (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }

function selectManteMaqui(id){
    return new Promise((resolve, reject) => {
        const sql = `
        SELECT 
        MM.mantenimiento_id AS 'Mantenimiento',
        Man.descripcion AS 'Descripcion del mantenimiento',
        MM.maquina_id AS 'Numero de Maquina',
        M.nombre AS 'Maquina',
        M.numeroserie AS 'Numero de Serie',
        M.tipo AS 'Tipo de Maquina',
        s.id AS 'Solicitud ID',
        s.fechaHora AS 'Fecha y hora de la solicitud'
        FROM ManteMaqui AS MM
        INNER JOIN Solicitud AS s ON MM.solicitud_id = s.id
        INNER JOIN Mantenimiento as Man ON Man.id = MM.mantenimiento_id
        INNER JOIN Maquina AS M ON M.id = MM.maquina_id
        WHERE MM.activo = 1 
        `;
        conexion.query(sql, [id], (err, results) => {
            if (err) {
                reject(err);
            } else {
                resolve(results);
            }
        });
    })
}

function selectAllManteMaqui(){
  return new Promise((resolve, reject) => {
      const sql = `
      SELECT 
      MM.mantenimiento_id AS 'Mantenimiento',
      Man.descripcion AS 'Descripcion del mantenimiento',
      MM.maquina_id AS 'Numero de Maquina',
      M.nombre AS 'Maquina',
      M.numeroserie AS 'Numero de Serie',
      M.tipo AS 'Tipo de Maquina',
      s.id AS 'Solicitud ID',
      s.fechaHora AS 'Fecha y hora de la solicitud'
      FROM ManteMaqui AS MM
      INNER JOIN Solicitud AS s ON MM.solicitud_id = s.id
      INNER JOIN Mantenimiento as Man ON Man.id = MM.mantenimiento_id
      INNER JOIN Maquina AS M ON M.id = MM.maquina_id
      WHERE MM.activo = 1 
      `;
      conexion.query(sql, (err, results) => {
          if (err) {
              reject(err);
          } else {
              resolve(results);
          }
      });
  })
}


function selectReporte(id) {
    return new Promise((resolve, reject) => {
      const sql = `
        SELECT
          Rep.id,
          Rep.descripcion,
          Rep.fechaInicio,
          Rep.fechaFinal,
          Rep.usuario_id,
          User.nombrePila
        FROM Reporte AS Rep
        INNER JOIN Usuario as User ON User.id = Rep.usuario_id
      `;
  
      conexion.query(sql, [id], (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }

  function selectAllReporte() {
    return new Promise((resolve, reject) => {
      const sql = `
        SELECT
          Rep.id,
          Rep.descripcion,
          Rep.fechaInicio,
          Rep.fechaFinal,
          Rep.usuario_id,
          User.nombrePila
        FROM Reporte AS Rep
        INNER JOIN Usuario as User ON User.id = Rep.usuario_id
      `;
  
      conexion.query(sql, (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }
  
  function selectTarea(table, id) {
    return new Promise((resolve, reject) => {
      const sql = `
      SELECT
      t.id AS id,
      s.descripcion AS 'Descripcion',
      s.fechaHora AS 'Fecha',
      Man.descripcion AS 'Mantenimiento',
      M.id AS 'Maquina ID',
      M.numeroserie AS 'Numero de Serie',
      M.nombre AS 'Maquina',
      s.departamento AS 'Departamento',
      FROM ${table} AS t
      INNER JOIN Solicitud AS s ON s.id = t.solicitud_id
      INNER JOIN Mantenimiento AS Man ON Man.id = t.mantenimiento_id
      INNER JOIN Maquina AS M ON M.id = s.maquina_id
      INNER JOIN Estado AS E ON E.id = t.estado_id
      WHERE t.activo = 0 AND s.usuario_id_resuelve = ${id}
      `;
  
      conexion.query(sql, [id], (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }

  function selectAllTarea() {
    return new Promise((resolve, reject) => {
      const sql = `
      SELECT
      t.id AS 'id',
      s.prioridad AS 'Prioridad',
      s.descripcion AS 'Descripcion',
      s.fechaHora AS 'Fecha y hora',
      M.id AS 'Maquina ID',
      M.numeroserie AS 'Numero de Serie',
      M.nombre AS 'Maquina'
      FROM Tarea AS t
      INNER JOIN Solicitud AS s ON s.id = t.solicitud_id
      INNER JOIN Mantenimiento AS Man ON Man.id = t.mantenimiento_id
      INNER JOIN ManteMaqui AS MM ON MM.mantenimiento_id = Man.id
      INNER JOIN Maquina AS M ON M.id = MM.maquina_id
      INNER JOIN Estado AS E ON E.id = t.estado_id
      WHERE t.activo = 0 
      `;
  
      conexion.query(sql, (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }
  
  function selectSolicitud(table, id) {
    return new Promise((resolve, reject) => {
      const sql = `
        SELECT 
          s.id AS 'Solicitud ID',
          s.prioridad AS 'Prioridad',
          MA.nombre AS 'Maquina',
          s.descripcion AS 'Descripcion del problema',
          s.fechaHora AS 'Fecha',
          CONCAT(US.nombrePila, ' ', US.apPat, ' ', US.apMat) AS 'Supervisor',
          s.departamento AS 'Departamento',
          s.tipo_mantenimiento AS 'Mantenimiento ID',
          M.descripcion AS 'Tipo de mantenimiento'
        FROM ${table} AS s
        INNER JOIN Usuario AS US ON US.id = s.usuario_id_solicita
        INNER JOIN Maquina AS MA ON MA.id = s.maquina_id
        INNER JOIN Mantenimiento AS M ON M.id = s.tipo_mantenimiento
        INNER JOIN Estado AS E ON E.id = s.estado_id
        WHERE s.activo = 1 AND US.id = ${id}
      `;
  
      conexion.query(sql, [id], (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }
  

  function selectAllSolicitud() {
    return new Promise((resolve, reject) => {
      const sql = `
      SELECT 
      s.id AS 'Solicitud ID',
      s.prioridad AS 'Prioridad',
      MA.nombre AS 'Maquina',
      s.descripcion AS 'Descripcion del problema',
      s.fechaHora AS 'Fecha',
      CONCAT(US.nombrePila, ' ', US.apPat, ' ', US.apMat) AS 'Supervisor',
      s.departamento AS 'Departamento',
      M.descripcion AS 'Tipo de mantenimiento'
      FROM Solicitud AS s
      INNER JOIN Usuario AS US ON US.id = s.usuario_id_solicita
      INNER JOIN Maquina AS MA ON MA.id = s.maquina_id
      INNER JOIN Mantenimiento AS M ON M.id = s.tipo_mantenimiento
      INNER JOIN Estado AS E ON E.id = s.estado_id
      WHERE s.activo = 1
      `;
  
      conexion.query(sql, (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }  
  
  function selectMisTareas(table, id) {
    return new Promise((resolve, reject) => {
      const sql = `
      SELECT
      T.id AS "id",
      S.prioridad AS "Prioridad",
      S.descripcion AS "Descripcion del problema",
      Man.descripcion AS "Mantenimiento",
      S.fechaHora AS "Fecha",
      S.departamento AS "Departamento",
      M.tipo AS "Maquina",
      M.numeroserie AS "Numero de Serie"
      FROM ${table} MT
      INNER JOIN Tarea T ON T.id = MT.tarea_id
      INNER JOIN Solicitud S ON S.id = T.solicitud_id
      INNER JOIN Mantenimiento Man ON Man.id = T.mantenimiento_id
      INNER JOIN Maquina M ON M.id = S.maquina_id
      WHERE T.activo = 1 AND MT.usuario_id = ${id}
      `;
  
      conexion.query(sql, [id], (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }

    function selectMisTareas(table, id) {
    return new Promise((resolve, reject) => {
      const sql = `
      SELECT
      T.id AS "id",
      S.prioridad AS "Prioridad",
      S.descripcion AS "Descripcion del problema",
      Man.descripcion AS "Mantenimiento",
      S.fechaHora AS "Fecha",
      S.departamento AS "Departamento",
      M.tipo AS "Maquina",
      M.numeroserie AS "Numero de Serie"
      FROM ${table} MT
      INNER JOIN Tarea T ON T.id = MT.tarea_id
      INNER JOIN Solicitud S ON S.id = T.solicitud_id
      INNER JOIN Mantenimiento Man ON Man.id = T.mantenimiento_id
      INNER JOIN Maquina M ON M.id = S.maquina_id
      WHERE T.activo = 1 AND MT.usuario_id = ${id}
      `;
  
      conexion.query(sql, [id], (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }

  function selectAllMisTareas() {
    return new Promise((resolve, reject) => {
      const sql = `
      SELECT
      T.id AS "id",
      S.prioridad AS "Prioridad",
      S.descripcion AS "Descripcion del problema",
      S.fechaHora AS "Fecha",
      S.departamento AS "Departamento",
      M.tipo AS "Maquina",
      Man.descripcion AS "Mantenimiento",
      M.numeroserie AS "Numero de Serie"
      FROM MisTareas MT
      INNER JOIN Tarea T ON T.id = MT.tarea_id
      INNER JOIN Solicitud S ON S.id = T.solicitud_id
      INNER JOIN Mantenimiento Man ON Man.id = T.mantenimiento_id
      INNER JOIN ManteMaqui MM ON MM.mantenimiento_id = Man.id
      INNER JOIN Maquina M ON M.id = MM.maquina_id
      WHERE T.activo = 1
      `;
  
      conexion.query(sql,(err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }

  function selectProblema(id) {
    return new Promise((resolve, reject) => {
      const sql = `
      SELECT
      Problema.id,
      Problema.descripcion,
      Problema.maquina_id,
      Maqui.nombre
      FROM Problema AS Problema
      INNER JOIN Maquina as Maqui ON Maqui.id = Problema.maquina_id
      WHERE Maqui.activo = 1
      `;
  
      conexion.query(sql, [id], (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }

  function selectAllProblema() {
    return new Promise((resolve, reject) => {
      const sql = `
      SELECT
      Problema.id,
      Problema.descripcion,
      Problema.maquina_id,
      Maqui.nombre
      FROM Problema AS Problema
      INNER JOIN Maquina as Maqui ON Maqui.id = Problema.maquina_id
      WHERE Maqui.activo = 1
      `;
  
      conexion.query(sql, (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }






module.exports = {
    all,
    one,
    add,
    disable,
    eraseone,
    query,
    changePassword,
    selectEmpleado,
    selectSupervisor,
    selectAllSupervisor,
    selectManteMaqui,
    selectAllManteMaqui,
    selectReporte,
    selectAllReporte,
    selectTarea,
    selectAllTarea,
    selectSolicitud,
    selectAllSolicitud,
    selectMisTareas,
    selectAllMisTareas,
    selectProblema,
    selectAllProblema
}